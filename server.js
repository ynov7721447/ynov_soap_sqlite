const express = require('express');
const cors = require('cors');
const soap = require('soap');
const fs = require('fs');
const { service, xml } = require('./soap-service');

const app = express();
app.use(cors());
app.use(express.static('public'));

app.listen(3000, () => {
    console.log('Server is running on port 3000');
    const wsdlPath = '/todo';
    soap.listen(app, wsdlPath, service, xml);
    console.log(`SOAP service available at http://localhost:3000${wsdlPath}?wsdl`);
});
