const db = require('./db');

const service = {
    TodoService: {
        TodoPort: {
            getTodoLists: (args, callback) => {
                db.all("SELECT * FROM todolists", [], (err, rows) => {
                    if (err) {
                        callback(err);
                    } else {
                        callback(null, { todoLists: rows });
                    }
                });
            },
            addTodoList: (args, callback) => {
                db.run("INSERT INTO todolists (name) VALUES (?)", [args.name], function (err) {
                    if (err) {
                        callback(err);
                    } else {
                        callback(null, { id: this.lastID });
                    }
                });
            },
            deleteTodoList: (args, callback) => {
                db.run("DELETE FROM todolists WHERE id = ?", [args.id], function (err) {
                    if (err) {
                        callback(err);
                    } else {
                        db.run("DELETE FROM todos WHERE list_id = ?", [args.id], function (err) {
                            if (err) {
                                callback(err);
                            } else {
                                callback(null, { deleted: this.changes });
                            }
                        });
                    }
                });
            },
            getTodos: (args, callback) => {
                db.all("SELECT * FROM todos WHERE list_id = ?", [args.listId], (err, rows) => {
                    if (err) {
                        callback(err);
                    } else {
                        callback(null, { todos: rows });
                    }
                });
            },
            addTodo: (args, callback) => {
                db.run("INSERT INTO todos (list_id, item) VALUES (?, ?)", [args.listId, args.item], function (err) {
                    if (err) {
                        callback(err);
                    } else {
                        callback(null, { id: this.lastID });
                    }
                });
            },
            deleteTodo: (args, callback) => {
                db.run("DELETE FROM todos WHERE id = ?", [args.id], function (err) {
                    if (err) {
                        callback(err);
                    } else {
                        callback(null, { deleted: this.changes });
                    }
                });
            },
            clearTodos: (args, callback) => {
                db.run("DELETE FROM todos WHERE list_id = ?", [args.listId], function (err) {
                    if (err) {
                        callback(err);
                    } else {
                        callback(null, { cleared: this.changes });
                    }
                });
            }
        }
    }
};

const xml = require('fs').readFileSync('todolist.wsdl', 'utf8');

module.exports = { service, xml };
