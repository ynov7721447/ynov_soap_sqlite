let currentLists = {};
let zIndexCounter = 1;
let dragItem = null;
let offsetX = 0;
let offsetY = 0;

async function fetchTodoLists() {
    const response = await fetch('http://localhost:3000/todo', {
        method: 'POST',
        headers: { 'Content-Type': 'text/xml' },
        body: `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ex="http://www.example.org/todo">
                    <soapenv:Header/>
                    <soapenv:Body>
                        <ex:getTodoListsRequest/>
                    </soapenv:Body>
                </soapenv:Envelope>`
    });
    const text = await response.text();
    const parser = new DOMParser();
    const xmlDoc = parser.parseFromString(text, 'text/xml');
    const todoLists = Array.from(xmlDoc.getElementsByTagName('todoLists')).map(list => ({
        id: list.getElementsByTagName('id')[0].textContent,
        name: list.getElementsByTagName('name')[0].textContent
    }));
    const todoSectionsElement = document.getElementById('todoSections');
    todoSectionsElement.innerHTML = '';
    todoLists.forEach(list => createTodoSection(list.id, list.name));
}

async function addTodoList() {
    const name = document.getElementById('newListName').value;
    if (!name) return;
    const response = await fetch('http://localhost:3000/todo', {
        method: 'POST',
        headers: { 'Content-Type': 'text/xml' },
        body: `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ex="http://www.example.org/todo">
                    <soapenv:Header/>
                    <soapenv:Body>
                        <ex:addTodoListRequest>
                            <ex:name>${name}</ex:name>
                        </ex:addTodoListRequest>
                    </soapenv:Body>
                </soapenv:Envelope>`
    });
    const text = await response.text();
    const parser = new DOMParser();
    const xmlDoc = parser.parseFromString(text, 'text/xml');
    const newListId = xmlDoc.getElementsByTagName('id')[0].textContent;
    createTodoSection(newListId, name);
    document.getElementById('newListName').value = '';
}

function createTodoSection(id, name) {
    const colors = ['#ffeb3b', '#ff5722', '#4caf50', '#03a9f4', '#e91e63'];
    const randomColor = colors[Math.floor(Math.random() * colors.length)];

    const todoSectionsElement = document.getElementById('todoSections');
    const todoSection = document.createElement('div');
    todoSection.id = `todoSection-${id}`;
    todoSection.className = 'post-it';
    todoSection.style.backgroundColor = randomColor;
    todoSection.style.display = 'block';
    todoSection.innerHTML = `
        <button class="delete-btn" onclick="deleteTodoList('${id}')">✕</button>
        <h2>${name}</h2>
        <input type="text" id="newTodo-${id}" class="form-control" placeholder="New Todo">
        <button class="btn btn-success btn-sm" onclick="addTodo('${id}')">Add Todo</button>
        <button class="btn btn-warning btn-sm" onclick="clearTodos('${id}')">Clear Todos</button>
        <ul id="todoList-${id}" class="list-group mt-2"></ul>
    `;
    todoSection.draggable = true;
    todoSection.ondragstart = (event) => dragStart(event, id);
    todoSection.ondragend = dragEnd;
    todoSectionsElement.appendChild(todoSection);
    fetchTodos(id);
}

function deleteTodoList(id) {
    fetch(`http://localhost:3000/todo`, {
        method: 'POST',
        headers: { 'Content-Type': 'text/xml' },
        body: `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ex="http://www.example.org/todo">
                    <soapenv:Header/>
                    <soapenv:Body>
                        <ex:deleteTodoListRequest>
                            <ex:listId>${id}</ex:listId>
                        </ex:deleteTodoListRequest>
                    </soapenv:Body>
                </soapenv:Envelope>`
    }).then(() => {
        const todoSection = document.getElementById(`todoSection-${id}`);
        todoSection.parentNode.removeChild(todoSection);
    });
}

async function fetchTodos(listId) {
    const response = await fetch('http://localhost:3000/todo', {
        method: 'POST',
        headers: { 'Content-Type': 'text/xml' },
        body: `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ex="http://www.example.org/todo">
                    <soapenv:Header/>
                    <soapenv:Body>
                        <ex:getTodosRequest>
                            <ex:listId>${listId}</ex:listId>
                        </ex:getTodosRequest>
                    </soapenv:Body>
                </soapenv:Envelope>`
    });
    const text = await response.text();
    const parser = new DOMParser();
    const xmlDoc = parser.parseFromString(text, 'text/xml');
    const todos = Array.from(xmlDoc.getElementsByTagName('todos')).map(todo => ({
        id: todo.getElementsByTagName('id')[0].textContent,
        item: todo.getElementsByTagName('item')[0].textContent
    }));
    const todoList = document.getElementById(`todoList-${listId}`);
    todoList.innerHTML = '';
    todos.forEach(todo => {
        const li = document.createElement('li');
        li.textContent = todo.item;
        li.dataset.id = todo.id;
        li.className = 'list-group-item';
        li.onclick = () => deleteTodo(todo.id, listId);
        todoList.appendChild(li);
    });
}

async function addTodo(listId) {
    const item = document.getElementById(`newTodo-${listId}`).value;
    if (!item) return;
    const response = await fetch('http://localhost:3000/todo', {
        method: 'POST',
        headers: { 'Content-Type': 'text/xml' },
        body: `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ex="http://www.example.org/todo">
                    <soapenv:Header/>
                    <soapenv:Body>
                        <ex:addTodoRequest>
                            <ex:listId>${listId}</ex:listId>
                            <ex:item>${item}</ex:item>
                        </ex:addTodoRequest>
                    </soapenv:Body>
                </soapenv:Envelope>`
    });
    const text = await response.text();
    const parser = new DOMParser();
    const xmlDoc = parser.parseFromString(text, 'text/xml');
    const newTodoId = xmlDoc.getElementsByTagName('id')[0].textContent;

    const todoList = document.getElementById(`todoList-${listId}`);
    const li = document.createElement('li');
    li.textContent = item;
    li.dataset.id = newTodoId;
    li.className = 'list-group-item';
    li.onclick = () => deleteTodo(newTodoId, listId);
    todoList.appendChild(li);

    document.getElementById(`newTodo-${listId}`).value = '';
}

async function deleteTodo(id, listId) {
    await fetch('http://localhost:3000/todo', {
        method: 'POST',
        headers: { 'Content-Type': 'text/xml' },
        body: `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ex="http://www.example.org/todo">
                    <soapenv:Header/>
                    <soapenv:Body>
                        <ex:deleteTodoRequest>
                            <ex:id>${id}</ex:id>
                        </ex:deleteTodoRequest>
                    </soapenv:Body>
                </soapenv:Envelope>`
    });
    const todoList = document.getElementById(`todoList-${listId}`);
    const todoItem = todoList.querySelector(`li[data-id='${id}']`);
    todoList.removeChild(todoItem);
}

async function clearTodos(listId) {
    await fetch('http://localhost:3000/todo', {
        method: 'POST',
        headers: { 'Content-Type': 'text/xml' },
        body: `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ex="http://www.example.org/todo">
                    <soapenv:Header/>
                    <soapenv:Body>
                        <ex:clearTodosRequest>
                            <ex:listId>${listId}</ex:listId>
                        </ex:clearTodosRequest>
                    </soapenv:Body>
                </soapenv:Envelope>`
    });
    const todoList = document.getElementById(`todoList-${listId}`);
    todoList.innerHTML = '';
}

function dragStart(event, listId) {
    dragItem = document.getElementById(`todoSection-${listId}`);
    offsetX = event.clientX - dragItem.getBoundingClientRect().left;
    offsetY = event.clientY - dragItem.getBoundingClientRect().top;
    event.dataTransfer.setData('text/plain', listId);
    dragItem.style.zIndex = ++zIndexCounter;
}

function dragEnd(event) {
    const element = dragItem;
    const x = event.clientX - offsetX;
    const y = event.clientY - offsetY;
    element.style.left = `${x}px`;
    element.style.top = `${y}px`;
    dragItem = null;
}

document.addEventListener('DOMContentLoaded', fetchTodoLists);
